In seinem Ziel Vertrauen und Rechtssicherheit bei Open Source zu schaffen, setzt openCode bei der Distribution von Binaries/Artefakten/Executables auf SBOMs.

![Overview](overview.png)


Als erstes Format setzt openCode dabei auf "SPDX". Dieses Repository soll Hilfestellungen, Code/Config-Snippets und Beispiele liefern, wie ein valides SPDX-Dokument zu einem gegebenen Artefakt erzeugt werden kann.
