# This file generates a basic SBOM Artifact from the given Source Directory
# "ort" container can be obtained from

# cd <dir_to_scan>
# bash ort_scan.sh <output_dir> 

# git clone https://github.com/oss-review-toolkit/ort.git
# cd ort
# bash scripts/docker_build.sh

#---
#excludes:
#  scopes:
#  - pattern: "devDependencies"
#    reason: "DEV_DEPENDENCY_OF"
#    comment: "Packages for development only."


set -x
#set -e


dir_out=$1

docker run -v $PWD:/project -v  $dir_out:/project_out ort --info analyze -i /project -o /project_out
docker run -v $PWD:/project -v  $dir_out:/project_out ort --info scan -i /project_out/analyzer-result.yml -o /project_out
docker run -v $PWD:/project -v  $dir_out:/project_out ort --info report -i /project_out/scan-result.yml -o /project_out -f SpdxDocument,StaticHtml

